package com.loadhbase;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Table;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.hbase.client.Connection;
import org.apache.hadoop.hbase.client.ConnectionFactory;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;



public class Loadhbase {
	 public static void main(String[] args) throws IOException {

	      // Instantiating Configuration class
	      Configuration config = HBaseConfiguration.create();

	      // Instantiating Hbase Table class
	      Connection connection = ConnectionFactory.createConnection(config);
	      Table hTable = connection.getTable(TableName.valueOf("user"));
	      Table hTable2 = connection.getTable(TableName.valueOf("book"));

	      System.out.println("connection establishment done");
	      System.out.println("Htable value" + hTable.toString());
	      try{
	      //Read Json file
		  JSONParser parser = new JSONParser();
		  JSONObject jObj;
		  
		  FileReader fr= new FileReader("/usr/local/RecommendationEngine/input/Books_5.json");
		  System.out.println("File read done");
		  BufferedReader buffer = new BufferedReader(fr);
		  
		  String currentRec;
		  int i=1;
		  
		  while((currentRec = buffer.readLine())!=null){
		  jObj = (JSONObject) parser.parse(currentRec);
		  
		  System.out.println("File put start for :" + jObj.get("reviewerID").toString());
	      // Instantiating Put class
	      // accepts a row name.
	      Put p = new Put(Bytes.toBytes(jObj.get("reviewerID").toString()));
	      
	      Put p1 = new Put(Bytes.toBytes(jObj.get("asin").toString()));
	      // adding values using addColumn() method
	      // accepts column family name, qualifier/row name ,value (from JSON)
	      
	      //Review Details
	      try{
	      p.addColumn(Bytes.toBytes("user details"),
	      Bytes.toBytes("reviewerName"),Bytes.toBytes(jObj.get("reviewerName").toString()));
	      
	      p.addColumn(Bytes.toBytes("book details"),
	      Bytes.toBytes("asin"),Bytes.toBytes(jObj.get("asin").toString()));
	      
	      //Review Details
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("helpful"),Bytes.toBytes(jObj.get("helpful").toString()));
	      
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("reviewText"),Bytes.toBytes(jObj.get("reviewText").toString()));
	      
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("overall"),Bytes.toBytes(jObj.get("overall").toString()));
	      
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("summary"),Bytes.toBytes(jObj.get("summary").toString()));

	      
	      //Supplementary Data
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("unixReviewTime"),Bytes.toBytes(jObj.get("unixReviewTime").toString()));
	      p.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("reviewTime"),Bytes.toBytes(jObj.get("reviewTime").toString()));

	      
	      // Saving the put Instance to the HTable.
	      hTable.put(p);
	      System.out.println("Row inserted");
	      
	      
	      p1.addColumn(Bytes.toBytes("user details"),
	      Bytes.toBytes("reviewerID"),Bytes.toBytes(jObj.get("reviewerID").toString()));
	      
	      p1.addColumn(Bytes.toBytes("user details"),
	      Bytes.toBytes("reviewerName"),Bytes.toBytes(jObj.get("reviewerName").toString()));
	    	      
	      //Review Details
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("helpful"),Bytes.toBytes(jObj.get("helpful").toString()));
	      
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("reviewText"),Bytes.toBytes(jObj.get("reviewText").toString()));
	      
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("overall"),Bytes.toBytes(jObj.get("overall").toString()));
	      
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("summary"),Bytes.toBytes(jObj.get("summary").toString()));

	      
	      //Supplementary Data
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("unixReviewTime"),Bytes.toBytes(jObj.get("unixReviewTime").toString()));
	      p1.addColumn(Bytes.toBytes("review details"),
	      Bytes.toBytes("reviewTime"),Bytes.toBytes(jObj.get("reviewTime").toString()));

	      
	      // Saving the put Instance to the HTable.
	      hTable2.put(p1);
	      }catch(NullPointerException ne){
	    	  System.out.println("Program hit - Null pointer exception");
	      }
		  }
	      
	      }catch(Exception e){
	    	  System.out.println(e);
	    	  e.printStackTrace();
	      }
	      // closing HTable
	      hTable.close();
	      hTable2.close();
	   }
}
